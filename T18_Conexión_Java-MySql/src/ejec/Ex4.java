package ejec;

import conect.Conexio;

public class Ex4 {
	public void ejecutar() {
		Conexio dbCon1= new Conexio();
		String db1="UD18Ex4";
		dbCon1.createDB(db1);
		//Crear tabla peliculas
		String crearPeliculas="CREATE TABLE `Peliculas` (\r\n" + 
				"  `Codigo` int NOT NULL,\r\n" + 
				"  `Nombre` varchar(100) NOT NULL,\r\n" + 
				"  `CalificacionEdad` int NOT NULL,\r\n" + 
				"  PRIMARY KEY (`Codigo`)\r\n" + 
				")";
		dbCon1.createTable(db1, crearPeliculas);
		//Crear tabla salas
		String crearSalas = "CREATE TABLE `Salas` (\r\n" + 
				"  `Codigo` int NOT NULL,\r\n" + 
				"  `Nombre` varchar(100) NOT NULL,\r\n" + 
				"  `Pelicula` int,\r\n" + 
				"  PRIMARY KEY (`Codigo`),\r\n" + 
				"  UNIQUE KEY `Nombre_UNIQUE` (`Nombre`),\r\n" + 
				"  KEY `FK_SALAS_PELICULAS_idx` (`Pelicula`),\r\n" + 
				"  CONSTRAINT `FK_SALAS_PELICULAS` FOREIGN KEY (`Pelicula`) REFERENCES `Peliculas` (`Codigo`) ON DELETE SET NULL ON UPDATE NO ACTION\r\n" + 
				")";
		dbCon1.createTable(db1, crearSalas);
		//Insertar Peliculas
		dbCon1.insertData(db1, "INSERT INTO `Peliculas` (`Codigo`, `Nombre`, `CalificacionEdad`) VALUES ('1', 'Alien', '18')");
		dbCon1.insertData(db1, "INSERT INTO `Peliculas` (`Codigo`, `Nombre`, `CalificacionEdad`) VALUES ('2', 'Star wars', '7')");
		dbCon1.insertData(db1, "INSERT INTO `Peliculas` (`Codigo`, `Nombre`, `CalificacionEdad`) VALUES ('3', 'Spiderman', '7')");
		dbCon1.insertData(db1, "INSERT INTO `Peliculas` (`Codigo`, `Nombre`, `CalificacionEdad`) VALUES ('4', 'Toy Story', '3')");
		dbCon1.insertData(db1, "INSERT INTO `Peliculas` (`Codigo`, `Nombre`, `CalificacionEdad`) VALUES ('5', 'Sharknado', '18')");
		//insertar Salas
		dbCon1.insertData(db1, "INSERT INTO `Salas` (`Codigo`, `Nombre`, `Pelicula`) VALUES ('1', 'Jupiter', 5)");
		dbCon1.insertData(db1, "INSERT INTO `Salas` (`Codigo`, `Nombre`, `Pelicula`) VALUES ('2', 'Marte', 2)");
		dbCon1.insertData(db1, "INSERT INTO `Salas` (`Codigo`, `Nombre`, `Pelicula`) VALUES ('3', 'Saturno', 4)");
		dbCon1.insertData(db1, "INSERT INTO `Salas` (`Codigo`, `Nombre`, `Pelicula`) VALUES ('4', 'Urano', 3)");
		dbCon1.insertData(db1, "INSERT INTO `Salas` (`Codigo`, `Nombre`, `Pelicula`) VALUES ('5', 'Venus', 1)");
		//Cerrar conexion
		dbCon1.closeConnection();
	}
}
