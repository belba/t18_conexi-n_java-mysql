package ejec;

import conect.Conexio;

public class Ex9 {
	public void ejecutar() {
		Conexio dbCon1= new Conexio();
		String db1="UD18Ex9";
		dbCon1.createDB(db1);
		//Crear tabla facultad
		String crearFacultad="CREATE TABLE `facultad` (\r\n" + 
				"  `codigo` int NOT NULL,\r\n" + 
				"  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,\r\n" + 
				"  PRIMARY KEY (`codigo`),\r\n" + 
				"  UNIQUE KEY `codigo_UNIQUE` (`codigo`)\r\n" + 
				")";
		dbCon1.createTable(db1, crearFacultad);
		//Crear tabla investigadores
		String crearInvestigadores = "CREATE TABLE `investigadores` (\r\n" + 
				"  `dni` varchar(8) NOT NULL,\r\n" + 
				"  `nomApelsFacultad` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,\r\n" + 
				"  `facultad` int DEFAULT NULL,\r\n" + 
				"  PRIMARY KEY (`dni`),\r\n" + 
				"  UNIQUE KEY `dni_UNIQUE` (`dni`),\r\n" + 
				"  KEY `fk_codigo_facultad_idx` (`facultad`),\r\n" + 
				"  CONSTRAINT `fk_codigo_facultad` FOREIGN KEY (`facultad`) REFERENCES `facultad` (`codigo`)\r\n" + 
				")";
		dbCon1.createTable(db1, crearInvestigadores);
		//Crear tabla Equipos
		String crearEquipos = "CREATE TABLE `equipos` (\r\n" + 
				"  `numSerie` char(4) NOT NULL,\r\n" + 
				"  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,\r\n" + 
				"  `facultad` int DEFAULT NULL,\r\n" + 
				"  PRIMARY KEY (`numSerie`),\r\n" + 
				"  UNIQUE KEY `numSerie_UNIQUE` (`numSerie`),\r\n" + 
				"  KEY `fk_codigo_facultad_idx` (`facultad`),\r\n" + 
				"  CONSTRAINT `fk_codi_facultad` FOREIGN KEY (`facultad`) REFERENCES `facultad` (`codigo`)\r\n" + 
				")";
		dbCon1.createTable(db1, crearEquipos);
		//Crear tabla Reservas
		String crearReserva = "CREATE TABLE `reserva` (\r\n" + 
				"  `dni` varchar(8) NOT NULL,\r\n" + 
				"  `numSerie` char(4) NOT NULL,\r\n" + 
				"  `comienzo` datetime NOT NULL,\r\n" + 
				"  `fin` datetime DEFAULT NULL,\r\n" + 
				"  PRIMARY KEY (`dni`,`numSerie`),\r\n" + 
				"  KEY `fk_numeroSerie_equipos_idx` (`numSerie`),\r\n" + 
				"  CONSTRAINT `fk_dni_investigadores` FOREIGN KEY (`dni`) REFERENCES `investigadores` (`dni`) ON DELETE CASCADE ON UPDATE CASCADE,\r\n" + 
				"  CONSTRAINT `fk_numeroSerie_equipos` FOREIGN KEY (`numSerie`) REFERENCES `equipos` (`numSerie`) ON DELETE CASCADE ON UPDATE CASCADE\r\n" + 
				")";
		dbCon1.createTable(db1, crearReserva);
		
		//Insertar Facultad
		dbCon1.insertData(db1, "INSERT INTO `facultad` VALUES (1,'FACULTAD SANTA MARIA'),(2,'IES SAN SALVADOR IIV'),(3,'IES JAUMA IIIV'),(4,'INSTITUT MARIA FORTUN'),(5,'FACULTAD JUAN II')");
		//insertar Investigadores
		dbCon1.insertData(db1, "INSERT INTO `investigadores` VALUES ('9000001B','JUAN JOSE',1),('9000002B','DON MARIO PEREZ',2),('9000003B','MARIA SANTOS',1),('9000004B','SAMUEL',4),('9000006B','FATIMA IBN SOLTAN',3)");
		
		//Insertar Equipos
		dbCon1.insertData(db1, "INSERT INTO `equipos` VALUES ('2C44','junior 3',5),('3E1D','junior 4',3),('A112','equipo 1',1),('B333','junior 2',2),('B33R','junior 7',4)");
		//Insertar Reserva
		dbCon1.insertData(db1, "INSERT INTO `reserva` VALUES ('9000001B','A112','2019-10-02 00:00:00','2020-11-08 00:00:00'),('9000001B','B333','2019-05-02 00:00:00','2020-11-20 00:00:00'),('9000002B','3E1D','2019-12-20 00:00:00','2020-02-22 00:00:00'),('9000003B','2C44','2019-01-02 00:00:00','2020-04-14 00:00:00'),('9000004B','2C44','2019-05-09 00:00:00','2020-06-13 00:00:00')");
		//Cerrar conexion
		dbCon1.closeConnection();
	}
}
