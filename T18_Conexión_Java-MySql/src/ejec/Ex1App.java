package ejec;

import conect.Conexio;

public class Ex1App {
	public void ex1() {
		//Crear base de datos
		Conexio dbCon1= new Conexio();
		String db1="UD18Ex1";
		dbCon1.createDB(db1);
		//Crear tabla fabricantes
		String crearFabricantes="CREATE TABLE `fabricantes` (\r\n" + 
				"  `codigo` int NOT NULL,\r\n" + 
				"  `nombre` varchar(45) DEFAULT NULL,\r\n" + 
				"  PRIMARY KEY (`codigo`),\r\n" + 
				"  UNIQUE KEY `codigo_UNIQUE` (`codigo`)\r\n" + 
				")";
		//Crear tabla articulos
		dbCon1.createTable(db1, crearFabricantes);
		String crearArticulos="CREATE TABLE `articulos` (\r\n" + 
				"  `codigo` int NOT NULL,\r\n" + 
				"  `nombre` varchar(45) NOT NULL,\r\n" + 
				"  `precio` int NOT NULL,\r\n" + 
				"  `codigo_fabricante` int DEFAULT NULL,\r\n" + 
				"  PRIMARY KEY (`codigo`),\r\n" + 
				"  UNIQUE KEY `codigo_UNIQUE` (`codigo`) /*!80000 INVISIBLE */,\r\n" + 
				"  KEY `codigo_fabricante_idx` (`codigo_fabricante`),\r\n" + 
				"  CONSTRAINT `codigo_fabricante` FOREIGN KEY (`codigo_fabricante`) REFERENCES `fabricantes` (`codigo`)\r\n" + 
				")";
		dbCon1.createTable(db1, crearArticulos);
		//Insertar fabricas
		dbCon1.insertData(db1, "INSERT INTO `fabricantes` (`codigo`, `nombre`) VALUES ('1', 'Danone')");
		dbCon1.insertData(db1, "INSERT INTO `fabricantes` (`codigo`, `nombre`) VALUES ('2', 'Frigo')");
		dbCon1.insertData(db1, "INSERT INTO `fabricantes` (`codigo`, `nombre`) VALUES ('3', 'Pepsi')");
		dbCon1.insertData(db1, "INSERT INTO `fabricantes` (`codigo`, `nombre`) VALUES ('4', 'Cocacola')");
		dbCon1.insertData(db1, "INSERT INTO `fabricantes` (`codigo`, `nombre`) VALUES ('5', 'Kalise')");
		
		//Insertar articulos
		dbCon1.insertData(db1, "INSERT INTO `articulos` (`codigo`, `nombre`, `precio`, `codigo_fabricante`) VALUES ('1', 'yogurt', '1', '1')");
		dbCon1.insertData(db1, "INSERT INTO `articulos` (`codigo`, `nombre`, `precio`, `codigo_fabricante`) VALUES ('2', 'patatas', '2', '3')");
		dbCon1.insertData(db1, "INSERT INTO `articulos` (`codigo`, `nombre`, `precio`, `codigo_fabricante`) VALUES ('3', 'helado', '1', '5')");
		dbCon1.insertData(db1, "INSERT INTO `articulos` (`codigo`, `nombre`, `precio`, `codigo_fabricante`) VALUES ('4', 'calipo', '1', '2')");
		dbCon1.insertData(db1, "INSERT INTO `articulos` (`codigo`, `nombre`, `precio`, `codigo_fabricante`) VALUES ('5', 'refresco', '3', '4')");
		
		//Cerrar conexion
		dbCon1.closeConnection();
		
	}
}
